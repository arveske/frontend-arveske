$(document).ready(function(){
  $('.form1').on('change', function() {
   if($('.form1:checked').length > 3) {
       this.checked = false;
   } else {
		$(this).closest('tr').toggleClass("highlight", this.checked);
   }
});
});

$(document).ready(function(){
$('.form2').on('change', function() {
   if($('.form2:checked').length > 3) {
       this.checked = false;
   } else {
		$(this).closest('tr').toggleClass("highlight", this.checked);
   }
});
});

$(document).ready(function(){
$('.form3').on('change', function() {
   if($('.form3:checked').length > 3) {
       this.checked = false;
   } else {
		$(this).closest('tr').toggleClass("highlight", this.checked);
   }
});
});

$(document).ready(function(){
$('.form4').on('change', function() {
   if($('.form4:checked').length > 3) {
       this.checked = false;
   } else {
		$(this).closest('tr').toggleClass("highlight", this.checked);
   }
});
});


$(document).ready(function(){

    var $checkboxes = $('input[type="checkbox"]');
        
    $checkboxes.change(function(){
        var countCheckedCheckboxes = $checkboxes.filter('.form1:checked').length;
		f1 = countCheckedCheckboxes;
        $('#count-checked-checkboxes1').text(countCheckedCheckboxes);
    });
});

$(document).ready(function(){

    var $checkboxes = $('input[type="checkbox"]');
        
    $checkboxes.change(function(){
        var countCheckedCheckboxes = $checkboxes.filter('.form2:checked').length;
		f2 = countCheckedCheckboxes;
        $('#count-checked-checkboxes2').text(countCheckedCheckboxes);
    });
});

$(document).ready(function(){

    var $checkboxes = $('input[type="checkbox"]');
        
    $checkboxes.change(function(){
        var countCheckedCheckboxes = $checkboxes.filter('.form3:checked').length;
		f3 = countCheckedCheckboxes;
        $('#count-checked-checkboxes3').text(countCheckedCheckboxes);
    });
});

$(document).ready(function(){

    var $checkboxes = $('input[type="checkbox"]'); 
        
    $checkboxes.change(function(){
        var countCheckedCheckboxes = $checkboxes.filter('.form4:checked').length;
		f4 = countCheckedCheckboxes;
		disable();
        $('#count-checked-checkboxes4').text(countCheckedCheckboxes);
    });
});