		var currentTab = 0;
		var f1 = 0, f2 = 0, f3 = 0, f4 = 0;
		disable();
		showTab(currentTab);
		disable();
		
		function disable() {
		  if (f1 > 0 && f2 > 0 && f3 > 0 && f4 > 0) {
			document.getElementById("prevBtn").disabled = false;
		  } else {
			document.getElementById("prevBtn").disabled = true;
		  }
		}
		
		function showTab(n) {
		  var x = document.getElementsByClassName("tab");
		  if (x[n] != null){
          x[n].style.display = "block";
          }

		  if (n == -1) {
		  
			document.getElementById("prevBtn").style.display = "none";
		  } else {
			document.getElementById("prevBtn").style.display = "inline";
		  }
		  if (n == 3) {
			document.getElementById("nextBtn").innerHTML = "Valmis!";
		  } else {
			document.getElementById("nextBtn").innerHTML = "Järgmine teema";
		  }
		  fixStepIndicator(n)
		}

		function nextPrev(n) {
		  var x = document.getElementsByClassName("tab");
		  x[currentTab].style.display = "none";
		  currentTab = currentTab + n;
		  if (currentTab >= x.length) {
			document.getElementById("regForm").submit();
			return false;
		  }
		  showTab(currentTab);
		}
		
		function showNow(n) {
		  var x = document.getElementsByClassName("tab");
		  x[currentTab].style.display = "none";
		  currentTab = n;
		  if (currentTab >= x.length) {
			document.getElementById("regForm").submit();
			return false;
		  }
		  showTab(currentTab);
		}


		function fixStepIndicator(n) {
		  var i, x = document.getElementsByClassName("step");
		  for (i = 0; i < x.length; i++) {
			x[i].className = x[i].className.replace(" active", "");
		  }
		}
		
$(document).ready(function () {
    $("a").click(function () {
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top;
        if ($.browser.safari) {
            $('body').animate({ scrollTop: destination }, 1100); 
        } else {
            $('html').animate({ scrollTop: destination }, 1100);
        }
        return false; 
    });
});
